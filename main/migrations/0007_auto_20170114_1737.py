# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-14 17:37
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_auto_20170114_1737'),
    ]

    operations = [
        migrations.AlterField(
            model_name='add_trip',
            name='fk_log',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='main.Log'),
        ),
    ]
