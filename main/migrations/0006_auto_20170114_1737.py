# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-14 17:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20170114_1551'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trip_notes',
            name='created',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='trip_notes',
            name='modified',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
