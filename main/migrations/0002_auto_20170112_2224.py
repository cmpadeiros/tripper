# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-12 22:24
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import djgeojson.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('body', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Route',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('order', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Spot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('coordinates', djgeojson.fields.PointField()),
                ('fk_country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Country', unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Spot_Photo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='Spot_Type',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='Trip',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateField()),
                ('end', models.DateField()),
                ('fk_log', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Log', unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Trip_Notes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=128)),
                ('body', models.TextField()),
                ('created', models.DateTimeField()),
                ('modified', models.DateTimeField()),
                ('author', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='Trip_Photos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('link', models.URLField()),
            ],
        ),
        migrations.DeleteModel(
            name='Place',
        ),
        migrations.AddField(
            model_name='add_trip',
            name='fk_notes',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Trip_Notes'),
        ),
        migrations.AddField(
            model_name='add_trip',
            name='fk_photos',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Trip_Photos'),
        ),
        migrations.AddField(
            model_name='add_trip',
            name='fk_route',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Route'),
        ),
        migrations.AddField(
            model_name='spot',
            name='fk_photo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Spot_Photo', unique=True),
        ),
        migrations.AddField(
            model_name='spot',
            name='fk_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Spot_Type'),
        ),
        migrations.AddField(
            model_name='route',
            name='fk_spot',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Spot'),
        ),
    ]
