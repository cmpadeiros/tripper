from django.db import models

# Abstract classes to be inherited, representing common behaviours
# and avoiding code repetition


"""
Class to be inherited by other model classes
Defines the representation of the class (name)
"""

class NameableModel(models.Model):
    def __unicode__(self):
        return self.name

    class Meta:
        """
        To allow other classes to inherit from the above one
        """
        abstract = True

class TimeStampedModel(models.Model):
    """
    An abstract base class model that provides self
    updating ``created`` and ``modified`` fields.
    """
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True )
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        """
        To allow other classes to inherit from the above one
        """
        abstract = True

class DurationModel(models.Model):
    """
    An abstract base class model that provides a start and
    an end date.
    """
    start = models.DateField(null=True, blank=True)
    end = models.DateField(null=True, blank=True)

    class Meta:
        """
        To allow other classes to inherit from the above one
        """
        abstract = True
