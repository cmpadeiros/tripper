from django.db import models
from djgeojson.fields import PointField

from main.models.behaviours import TimeStampedModel, DurationModel, NameableModel

# This is the start of a new world!
# Create your models here.

"""
Class representing the spot type (ex. waterfall, city, ...)
"""
class Spot_Type(NameableModel):
    name = models.CharField(max_length=128)

"""
TODO: check if it is really needed
Class representing a country
"""
class Country(NameableModel):
    name = models.CharField(max_length=128)

"""
Class holding a URL pointing to a photo which will
identify a spot
"""
class Spot_Photo(models.Model):
    url = models.URLField()

"""
Class holding a URL pointing to the place where
we will save our photos (some cloud service?)
"""
class Trip_Photos(models.Model):
    link = models.URLField()

    def __unicode__(self):
        return self.link

"""
Class representing a spot, a named point in the map.
Several spots will make up a route.
"""
class Spot(NameableModel):
    name = models.CharField(max_length=128)
    coordinates = PointField()
    fk_type = models.ForeignKey(Spot_Type, null=True, blank=True)
    fk_country = models.OneToOneField(Country, null=True, blank=True)
    fk_photo = models.OneToOneField(Spot_Photo, null=True, blank=True)

"""
Class representing a route, an agreggation of spots. Routes
will make up trips and can be associated with different trips.
"""
class Route(NameableModel):
    name = models.CharField(max_length=128)
    fk_spot = models.ManyToManyField(Spot, through="Route_Spot")

"""
Class which represents the many to many connection between routes
and spots. Each spot will be given an order inside the route
"""
class Route_Spot(models.Model):
    fk_route = models.ForeignKey(Route, on_delete=models.CASCADE)
    fk_spot = models.ForeignKey(Spot, on_delete=models.CASCADE)
    order = models.IntegerField()


"""
Class representing a named add_trip, made up of several routes. Will
have the ability to hold a log, notes and photos
"""
class Trip(NameableModel, DurationModel):
    name = models.CharField(max_length=128)
    fk_route = models.ManyToManyField(Route, through="Trip_Route")
    fk_photos = models.ForeignKey(Trip_Photos, null=True, blank=True)

"""
Class which represents the many to many connection between routes
and trips.
"""
class Trip_Route(models.Model):
    fk_route = models.ForeignKey(Route, on_delete=models.CASCADE)
    fk_trip = models.ForeignKey(Trip, on_delete=models.CASCADE)

"""
Class representing general notes and reminders for each add_trip
"""
class Trip_Notes(TimeStampedModel, NameableModel):
    name = models.CharField(max_length=128)
    body = models.TextField()
    author = models.CharField(max_length=64)
    fk_trip = models.ForeignKey(Trip)

    def __unicode__(self):
        return "Note {0} of add_trip {1}".format(self.name, self.fk_trip.name)

"""
Class representing a log which we will use to document
each add_trip
"""
class Trip_Log(TimeStampedModel):
    body = models.TextField()
    fk_trip = models.OneToOneField(Trip)

    def __unicode__(self):
        return self.fk_trip.name + " log"