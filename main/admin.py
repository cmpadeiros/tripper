from django.contrib import admin
import main.models.models as main_model

# Register your models here.

@admin.register(main_model.Trip, main_model.Trip_Route, main_model.Route,
                main_model.Route_Spot, main_model.Spot_Type, main_model.Country,
                main_model.Spot_Photo, main_model.Trip_Log, main_model.Spot)
class DefaultAdmin(admin.ModelAdmin):
    pass
