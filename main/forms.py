from django.forms import ModelForm
from .models import Trip

# Generating the form directly from the model
class TripForm(ModelForm):
    class Meta:
        model = Trip
        fields = ['name', 'start', 'end']