import os
import django
import datetime

def setup_environment():

    # pointing the environment variable to the project settings
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tripper.settings")

    # setting up django
    django.setup()

if __name__ == "__main__":

    setup_environment()

    # my django models
    from main.models.models import *

    # mostrar todas as viagens
    print Trip.objects.all()

    # quantas viagens
    print Trip.objects.count()

    # quantos paises na viagem A
    viagem_a = Trip.objects.get(name="Viagem A")
    countries_a = Country.objects.filter(spot__route__trip=viagem_a)
    print countries_a.count()

    # locais do tipo cascata
    print Spot.objects.filter(fk_type__name="Cascata")

    # locais em portugal
    print Spot.objects.filter(fk_country__name="Portugal")

    # viagens entre 2016 e 2017, inclusive
    start_date = datetime.date(2016, 1, 1)
    end_date = datetime.date(2017, 1, 1)
    print Trip.objects.filter(start__range=(start_date, end_date))
