# Create your tests here.
import pytest
from main.models.models import Spot
from djgeojson.fields import PointField


@pytest.mark.django_db
def test_can_add_spot_and_retrieve_from_db():

    # Arrange
    test_spot = Spot.objects.create(
        name="Test add_trip",
        coordinates={'type': 'Point', 'coordinates': [0, 0]}
    )

    # Act
    test_spot.save()

    # Assert
    assert Spot.objects.first() == test_spot
