from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import TripForm
from .models import Trip

# Create your views here.
def welcome(request):
    return render(request, 'welcome.html')

def map(request):
    return render(request, 'map.html')

def add_trip(request):
    if request.method == 'POST':
        form = TripForm(request.POST)
        if form.is_valid():
            trip_name = request.POST.get('name')
            start_date = request.POST.get('start')
            end_date = request.POST.get('end')

            new_trip = Trip(name=trip_name, start=start_date, end=end_date)
            new_trip.save()

            return HttpResponseRedirect(reverse('map'))

    else:
        form = TripForm()

    return render(request, 'add_trip.html', {'form_trip': form})

def add_spot(request):
    return render(request, 'add_spot.html')
