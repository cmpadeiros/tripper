/**
 * Created by Miguel on 11/02/2017.
 */
var myMap = {
    map_instance: {},
    map_markers: [],

    /*
     Creates a leaflet map instance, initialized with the given parameters
     @param initial_latitude: float, initial latitude value for centering the map
     @param initial_longitude: float, initial longitude value for centering the map
     @param zoom: float, initial zoom level for the map
     */
    initMap: function initMap(initial_latitude, initial_longitude, zoom) {
        // set up the map
        this.map_instance = L.map('map').setView([initial_latitude, initial_longitude], zoom);
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(this.map_instance);
    },

    addSearchbar: function addSearchbar(){
      L.control.geocoder('mapzen-v3ejWt9').addTo(this.map_instance);
    },

    /*
     Marks a point in the map
     */

    addMarker: function (e) {
        L.marker(e.latlng).addTo(this);
    }
};